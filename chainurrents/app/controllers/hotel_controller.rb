class HotelController < ApplicationController
  def index
    if((params.has_key?(:errormessage) && params[:errormessage].present?))
      @errormessage = params[:errormessage]
    end
  end
  def search
      @message = ""

    if((params.has_key?(:city) && params[:city].present?) && (params.has_key?(:startDate) && params[:startDate].present?) && (params.has_key?(:endDate) && params[:endDate].present? ))
      @errormessage = ""
      @message = "Results for " + params[:city] + " between " + params[:startDate] + " to " + params[:endDate]
      @startDate = params[:startDate]
      @endDate = params[:endDate]
      @sameCityHotels = Hotel.where("(city = ? AND booking_start_date <= ? AND booking_end_date  <= ?) OR (city = ? AND booking_start_date  >=  ? AND booking_end_date >= ? )", params[:city], @startDate, @endDate, params[:city], @startDate, @endDate )
      Date.parse('2010-11-01') < Date.today


    else

      redirect_to search_path(:errormessage => "Please fill in all fields")

    end
  end
end
