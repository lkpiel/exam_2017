Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :hotel
  get '/' => 'hotel#index' , :as => :search
  get '/search' => 'hotel#search'

end
