Then(/^I fill in city with "([^"]*)"$/) do |arg1|
  fill_in 'city', with: 'Tallinn'
end

Then(/^I fill in start date with "([^"]*)"$/) do |arg1|
  fill_in 'startDate', with: '06/06/2017'
end

Then(/^I fill in end date with "([^"]*)"$/) do |arg1|
  fill_in 'endDate', with: '06/06/2017'
end

Then(/^I click search$/) do
  click_button("Search")
end
