# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

more_hotels = [

{:name => 'Tallinn Hotell', :city => 'Tallinn', :address => 'Tammsaare tee 123, 12917, Tallinn' , :cost => '55.5' , :area => '33', :floor => "2", :services => "Accommodates: 2  Bathrooms: 1 Beds: 1  TV: Yes", :check_in_time => "13:00", :booking_start_date => "05.05.2017", :booking_end_date => '07.05.2017'},
{:name => 'Tartu Hotell', :city => 'Tartu', :address => 'Liivi 2, 11111, Tartu' , :cost => '55.5' , :area => '33', :floor => "2", :services => "Accommodates: 2  Bathrooms: 1  Beds: 1  TV: Yes", :check_in_time => "13:00", :booking_start_date => "05.05.2017", :booking_end_date => '07.05.2017'},
{:name => 'Tallinn Hotell Viru', :city => 'Tallinn', :address => 'Tartu maantee 3, 12227, Tallinn' , :cost => '88.5' , :area => '33', :floor => "2", :services => "Accommodates: 3  Bathrooms: 1  Beds: 2  TV: NO", :check_in_time => "13:00", :booking_start_date => "01.06.2017", :booking_end_date => '30.09.2017'}
]

more_hotels.each do |hotel|
  Hotel.create!(hotel)
end
