require 'rails_helper'

RSpec.describe HotelController, type: :controller do
  before(:each) do
    @hotel1 = Hotel.create!(:name => 'Tallinn Hotell', :city => 'Tallinn', :address => 'Tammsaare tee 123, 12917, Tallinn' , :cost => '55.5' , :area => '33', :floor => "2", :services => "Accommodates: 2  Bathrooms: 1  Beds: 1  TV: Yes", :check_in_time => "13:00", :booking_start_date => "05.05.2017", :booking_end_date => '07.05.2017')
    @hotel2 = Hotel.create!(:name => 'Tartu Hotell', :city => 'Tartu', :address => 'Liivi 2, 11117, Tartu' , :cost => '55.5' , :area => '33', :floor => "2", :services => "Accommodates: 2  Bathrooms: 1  Beds: 1  TV: Yes", :check_in_time => "13:00", :booking_start_date => "05.05.2017", :booking_end_date => '7.05.2017')
    @hotel3 = Hotel.create!(:name => 'Tallinn Hotell Viru', :city => 'Tallinn', :address => 'Tartu maantee 3, 12227, Tallinn' , :cost => '88.5' , :area => '33', :floor => "2", :services => "Accommodates: 3  Bathrooms: 1  Beds: 2  TV: NO", :check_in_time => "13:00", :booking_start_date => "01.06.2017", :booking_end_date => '01.09.2017')

  end
  describe "GET" do
    it "returns http success for #index" do
      get :index
      expect(response).to have_http_status(:success)
    end
    it "returns http success for #search" do
      get :search, :city => "Tallinn",  :startDate => "06-06-2017", :endDate => "09-09-2017"
      expect(response).to have_http_status(:success)
    end
    it "creates a message for user depending on the traveller input" do
      get :search, :city => "Tallinn", :startDate => "06-06-2017", :endDate => "09-09-2017"
      expect(assigns(:message)).not_to be_nil
      expect(assigns(:message)).to eq("Results for Tallinn between 06-06-2017 to 09-09-2017")
    end
    it "creates a list of hotels where city is the same as user input" do
      get :search, :city => "Tallinn", :startDate => "06-06-2017", :endDate => "09-09-2017"
      expect(assigns(:sameCityHotels)).not_to be_nil
      expect(assigns(:sameCityHotels)).to include(@hotel1)
      expect(assigns(:sameCityHotels)).not_to include(@hotel2)
    end
    it "creates a list of hotels where city is the same as user input and only includes hotels where there is no booking made for user input" do
      get :search, :city => "Tallinn", :startDate => "01-01-2017", :endDate => "09-09-2017"
      expect(assigns(:sameCityHotels)).not_to be_nil
      expect(assigns(:sameCityHotels)).not_to include(@hotel1)
      expect(assigns(:sameCityHotels)).not_to include(@hotel2)
    end

  end
end
