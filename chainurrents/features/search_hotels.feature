Feature: Search for a hotel
  As a traveller
  So that I can see the list of available hotels
  I want to enter city and the time period

Scenario: I visi the search page
      Given I am on the searching page
      Then I should see "Enter city:"
      And I should see "Enter starting date:"
      And I should see "Enter end date:"
      And I should see "Search"
