class CreateHotels < ActiveRecord::Migration[5.0]
  def change
    create_table :hotels do |t|
      t.string :name
      t.string :city
      t.string :address
      t.float :cost
      t.float :area
      t.integer :floor
      t.string :services
      t.string :check_in_time
      t.date :booking_start_date
      t.date :booking_end_date

      t.timestamps
    end

  end
end
