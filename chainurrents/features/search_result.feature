Feature: Search for a hotel
  As a traveller
  So that I can book a hotel
  I want to see a list of available hotels

Scenario: I search for a hotel in Tallinn
    Given I am on the searching page
    Then I fill in city with "Tallinn"
    And I fill in start date with "06-06-2017"
    And I fill in end date with "10-06-2017"
    And I click search
    Then I should be on the result page
    And I should see "Results for Tallinn between 06/06/2017 to 06/06/2017"


Scenario: I don't fill in any fields
    Given I am on the searching page
    And I click search
    Then I should be on the searching page
    And I should see "Please fill in all fields"
